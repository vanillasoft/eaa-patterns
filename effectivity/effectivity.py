from dataclasses import dataclass, field
from datetime import date
from typing import Optional, List


@dataclass
class NamedObject:
    name: str = "No name"


@dataclass
class DateRange:
    start: date
    end: Optional[date]

    @classmethod
    def starting_on(cls, start: date):
        return cls(start=start, end=None)

    def includes(self, arg: date):
        if self.end is None:
            return self.start < arg
        return self.start < arg < self.end



class Company(NamedObject):
    pass


@dataclass
class Employment:
    company: Company
    effective: DateRange
    
    @classmethod
    def starting_on(cls, company: Company, start_date: date):
        return cls(company=company, effective=DateRange.starting_on(start_date))

    @classmethod
    def in_range(cls, company: Company, effective: DateRange):
        return cls(company=company, effective=effective)
  
  
    def is_effective_on(self, arg: date) -> bool:
        return self.effective.includes(arg)

    def end(self, end_date: date):
        self.effective = DateRange(start=self.effective.start, end=end_date)
  
    def set_effectivity(self, arg: DateRange):
        self.effective = arg

@dataclass
class Person(NamedObject):
    employments: List[Employment] = field(default_factory=lambda: [])

    def start_employment_in_company(self, company: Company, start_date: date):
        self.employments.append(Employment.starting_on(company=company, start_date=start_date))

    def add_employment(self, employment: Employment):
        self.employments.append(employment)

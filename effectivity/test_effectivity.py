from datetime import date
from effectivity import Person, Company, DateRange, Employment

class TestEmployments:

  
    def test_additive(self):
        duke = Person(name="Duke")
        india = Company(name="India")
        peninsular = Company(name="Peninsular")
        
        duke.start_employment_in_company(india, date(1999,12,1))
        duke.start_employment_in_company(peninsular, date(2000,4,1))
        duke.employments[0].end(date(2000,5,1))
        assert 2 == len(duke.employments)
        
        actual = None

        for employment in duke.employments:
            if employment.is_effective_on(date(2000,6,1)):
                actual = employment
                break
        
        
        assert actual is not None
        assert peninsular == actual.company


    def test_retro(self):
        duke = Person(name="Duke")
        india = Company(name="India")
        peninsular = Company(name="Peninsular")
        dublin = Company(name="Dublin")
        duke.start_employment_in_company(india, date(1999,12,1))
        duke.employments[0].end(date(2000,5,1))
        
        duke.start_employment_in_company(peninsular, date(2000,4,1))

        duke.employments[1].set_effectivity(DateRange.starting_on(date(2000,6,1)))
        duke.add_employment(Employment(dublin, DateRange(date(2000,5,1), date(2000,5,31))))

        april = None
        for employment in duke.employments:
            if employment.is_effective_on(date(2000,4,10)):
                april = employment
                break
        
        
        assert april is not None
        assert india == april.company

        may = None
        for employment in duke.employments:
            if employment.is_effective_on(date(2000,5,10)):
                may = employment
                break
        
        
        assert may is not None
        assert dublin == may.company
  
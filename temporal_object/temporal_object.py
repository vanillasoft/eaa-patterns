import dataclasses
from dataclasses import dataclass, field
from datetime import date
from typing import Any, Dict, Optional

@dataclass
class TemporalCollection:
    today: date = field(default_factory=lambda: date.today())
    contents: Dict[date, Any] = field(default_factory=lambda: {})

    def get(self, when: Optional[date] = None):
        if when is None:
            when = self.today
        this_date = self._get_previous_date(when)
        return self.contents[this_date]

    def put(self, item: Any, at: Optional[date]):
        if at is None:
            at = self.today
        self.contents[at] = item
        self.contents = dict(sorted(self.contents.items()))

    def get_in(self, from_date, to_date):
        start_date = self._get_previous_date(from_date)
        print(start_date)

        return {
                start: content for start, content in self.contents.items()
                if start >= start_date and start < to_date
            }


    def _get_previous_date(self, when: date):
        print(f"when is {when}")
        for this_date in reversed(self.contents.keys()):
            if this_date <= when:
                return this_date
        raise KeyError("no records that early")   




@dataclass
class CustomerVersion:
    name: str
    address: str
    credit_limit: float
    phone: str

@dataclass
class Customer:
    history: TemporalCollection = TemporalCollection()
    today: date = field(default_factory=lambda: date.today())

    @property
    def name(self):
        return self.current().name

    @name.setter
    def name(self, value):
        self.history.put(dataclasses.replace(self.current(), name=value))

    @property
    def address(self):
        return self.current().address

    @address.setter
    def address(self, value):
        self.history.put(dataclasses.replace(self.current(), address=value), self.today)

    def get_address(self, when: date):
        return self.history.get(when).address
    
    def set_address(self, value, when: date):
        self.history.put(dataclasses.replace(self.history.get(when), address=value), when)
    

    @property
    def credit_limit(self):
        return self.current().credit_limit

    @credit_limit.setter
    def credit_limit(self, value):
        self.history.put(dataclasses.replace(self.current(), credit_limit=value))

    def get_credit_limit(self, when: date):
        return self.history.get(when).credit_limit

    def set_credit_limit(self, value, when: date):
        self.history.put(dataclasses.replace(self.history.get(when), credit_limit=value), when)
  
    @property
    def phone(self):
        return self.current().phone

    @phone.setter
    def phone(self, value):
        self.history.put(dataclasses.replace(self.current(), phone=value))

    def phone(self, when: date):
        return self.history.get(when).phone

    def set_phone(self, value, when: date):
        self.history.put(dataclasses.replace(self.history.get(when), phone=value), when)
    
    def current(self):
        return self.history.get()

    def get_version_at(self, at):
        return self.history.get(at)

    def get_versions(self, start, end):
        return self.history.get_in(start, end)

    def set_version_at(self, version, at):
        self.history.put(version, at)

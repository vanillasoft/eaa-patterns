from datetime import date
from temporal_object import Customer, TemporalCollection, CustomerVersion

class TestCustomer:
    def test_simple(self):
        
        martin_v0 = CustomerVersion(
            name="Martin",
            address="Paris",
            credit_limit=50,
            phone="01"
            
        )
        martin = Customer(history=TemporalCollection(contents={date(1977, 12, 25): martin_v0}))
        
        
        martin.set_address("Londres", date(1998, 8, 23))
        martin.set_credit_limit(100, date(1998, 8, 23))
    
        assert 50 == martin.get_credit_limit(date(1997, 12, 25))
        assert 50 == martin.get_credit_limit(date(1998, 8, 22))
        assert 100 == martin.get_credit_limit(date(1998, 8, 23))
        assert 100 == martin.credit_limit

        assert "Paris" == martin.get_address(date(1997, 12, 25))
        assert "Paris" == martin.get_address(date(1998, 8, 22))
        assert "Londres" == martin.get_address(date(1998, 8, 23))
        assert "Londres" == martin.address

    def test_multiple(self):
        
        martin_v0 = CustomerVersion(
            name="Martin",
            address="Paris",
            credit_limit=50,
            phone="01"
            
        )
        martin = Customer(history=TemporalCollection(contents={date(1977, 12, 25): martin_v0}))

        martin.set_address("Londres", date(1998, 8, 23))
        martin.set_credit_limit(100, date(1998, 8, 23))

        martin.set_credit_limit(150, date(2000, 1 ,1))

        martin.set_credit_limit(200, date(2001, 1, 1))

        martin_evolution = martin.get_versions(date(1980, 1, 1), date(2000, 12, 25))

        assert len(martin_evolution) == 3
        assert list(martin_evolution.keys()) == [date(1977, 12, 25), date(1998, 8, 23), date(2000, 1, 1)]

  
        martin_evolution = martin.get_versions(date(1999, 1, 1), date(2000, 1, 2))
        assert len(martin_evolution) == 2
        assert list(martin_evolution.keys()) == [date(1998, 8, 23), date(2000, 1, 1)]

        martin_evolution = martin.get_versions(date(1999, 1, 1), date(2010, 1, 2))
        assert len(martin_evolution) == 3
        assert list(martin_evolution.keys()) == [date(1998, 8, 23), date(2000, 1, 1), date(2001, 1, 1)]

        martin_evolution = martin.get_versions(date(1998, 8, 23), date(2001, 1, 1))
        assert len(martin_evolution) == 2
        assert list(martin_evolution.keys()) == [date(1998, 8, 23), date(2000, 1, 1)]
